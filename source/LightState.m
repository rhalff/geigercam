classdef LightState
    %LIGHTSTATE State of traffic light visualization
    %   Implemented for three states
    
    % Copyright 2014- Thomas Auzinger <thomas@auzinger.name>
    
    %% Public enumeration
    enumeration
        Off
        Danger
        Safe
    end
    
end

