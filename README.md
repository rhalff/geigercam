# GeigerCam: Measuring Radioactivity with Webcams

This package contains the source code of a protoype implementation to use an HD webcam as radioactivity detector.

## References

- Link to official publication: <http://dx.doi.org/10.1145/2342896.2342949><br>Please cite it, if you use this program in your publication.
- Link to project homepage: <https://www.cg.tuwien.ac.at/research/publications/2012/Auzinger_2012_GeigerCam/>

## Prerequisites

This program was developed and tested on the following configuration:
- **Software:** Matlab R2013a, NVIDIA GeForce driver 320.00
- **Hardware:** NVIDIA GeForce GTX 680, Logitech C270 HD

Requirements to run this software:
- **Software:**
  - Matlab R2012b (or later)
  - Matlab Parallel Computing Toolbox
  - Matlab Image Acquisition Toolbox
- **Hardware:**
  - NVIDIA GPU with Compute Capability 1.3 (or higher)
  - Webcam (HD resolution recommended) modified according to our publication

## Contents

| Filename          | Description                                 |
| ----------------: | :------------------------------------------ |
| `README.md`       | This file                                   |
| `GUI.fig`         | Graphical user interface (GUI)              |
| `GUI.m`           | Event handlers for the GUI                  |
| `LightState.m`    | Enumeration for the traffic light graphics  |
| `Config.m`        | General configuration file                  |
| `GPGPU.m`         | Graphics processing unit (GPU) computations |
| `CameraControl.m` | Video stream management of the webcam       |
| `Hit.m`           | Analysis of a particle hit                  |

## Description

For the specified hard- and software, this code should run out of the box.
Otherwise, the parameters in `CamerControl.m` have to be adapted to the new camera.

The graphics card computations are used to accelerate the filtering operations.
Pure CPU computations could not keep up with the HD video stream.

This package assumes that the modifications of the camera have been executed according to our publication.
All computations are based on the fact that the video stream is essentially black with just the radioactive hits.
A fully illuminated image will not yield any useful results.

To evaluate the device noise (thermal noise and destroyed pixels), a initialization phase is required in which the camera is NOT exposed to radioactive radiation.

## Manual
1. Connect the webcam to the PC and use the preview of the *Image Acquisition Toolbox* to ensure its functionality.
2. Start the program by launching `GUI.m`.
3. Start the initialization procedure by pressing the printed circuit board icon.
   At this time, the camera must NOT be exposed to radioactive radiation.
4. After initialization, both the camera and the GPU should be correctly identified by the program.
5. Start the measurement by pressing the play button.
   At this time, the camera should be exposed to radioactive radiation.
6. During measurement, the GUI provides the following pieces of information:
    - Measurement box (top right): The total hit counts since the start of measurement and the average hit counts per second.
    - Impact images (center): Images of the identified hits by radioactive radiation. These should be small bright spots.
    - Result (bottom right): A traffic light indicates the presence (red) and absence (green) of radiation. No measurement activity (yellow) is shown separately.

## Contact

For questions and suggestions, create a GitLab issue or contact <thomas@auzinger.name>.

## Authors

- Thomas AUZINGER - [webpage](http://auzinger.name)

## License

This project is licensed under the GNU GPLv3 license - see the [LICENSE][license] file for details.

[license]: LICENSE

Copyright 2014- Thomas AUZINGER <thomas@auzinger.name>
