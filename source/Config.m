classdef Config
    %CONFIG Parameters
    %   All parameters for the program are stored here
    
    % Copyright 2014- Thomas Auzinger <thomas@auzinger.name>
    
    %% Constant public properties
    properties (Constant = true)
        FPS = 5;
        
        ProbabilityConvolutionFilter = [sqrt(2) 1 sqrt(2), 1 0 1, sqrt(2) 1 sqrt(2)];
        NoiseThreshold = 100;
        SmoothingAlpha = 0.01;
        
        InitializationFrameCount = Config.FPS * 10;
        
        HitImagesCountPerSide = 5;
        DangerThreshold = 2;
        AverageTimeHitCounts = 1;
        
        MinEnergy = 10;
    end
    
end

